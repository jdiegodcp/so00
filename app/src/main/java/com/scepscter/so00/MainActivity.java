package com.scepscter.so00;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MainActivity";

    //vars
    private ArrayList<String> mNames = new ArrayList<>();
    private ArrayList<String> mImageUrls = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d(TAG, "onCreate: Started");
        
        initImageBitmaps();
    }
    
    private void initImageBitmaps(){
        Log.d(TAG, "initImageBitmaps: preparing bitmaps.");

        mImageUrls.add("https://lp-cms-production.imgix.net/2019-06/70114953.jpg?auto=format&fit=crop&vib=20&sharp=10&ixlib=react-8.6.4&w=1600&q=40&dpr=1%201x,%20https://lp-cms-production.imgix.net/2019-06/70114953.jpg?auto=format&fit=crop&vib=20&sharp=10&ixlib=react-8.6.4&w=1600&q=40&dpr=2%202x,%20https://lp-cms-production.imgix.net/2019-06/70114953.jpg?auto=format&fit=crop&vib=20&sharp=10&ixlib=react-8.6.4&w=1600&q=40&dpr=3%203x,%20https://lp-cms-production.imgix.net/2019-06/70114953.jpg?auto=format&fit=crop&vib=20&sharp=10&ixlib=react-8.6.4&w=1600&q=40&dpr=4%204x,%20https://lp-cms-production.imgix.net/2019-06/70114953.jpg?auto=format&fit=crop&vib=20&sharp=10&ixlib=react-8.6.4&w=1600&q=40&dpr=5%205x");
        mNames.add("Cuzco");

        mImageUrls.add("https://www.gotokyo.org/en/spot/6/images/4507_1_1400x1100.png");
        mNames.add("Tokyo");

        initRecyclerView();
    }

    private void initRecyclerView(){
        Log.d(TAG, "initRecyclerView: init recyclerView.");
        RecyclerView recyclerView = findViewById(R.id.recyclerv_view);
        RecyclerViewAdapter adapter = new RecyclerViewAdapter(this,mNames,mImageUrls);
        recyclerView.setAdapter(adapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
    }
}
